---
title: "Lab 15 Solutions"
author: "Sterling Hayden"
date: "Fall 2021"
output:
  pdf_document: default
  html_document: default
urlcolor: blue
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, message = FALSE)
```


```{r, message = FALSE}
library(tidyverse)
library(here)
library(gapminder)
library(forcats)
```

# Getting Started

**Homework must be submitted as a single pdf file which is the output of the RMarkdown file. Please compile the pdf and upload it to Gradescope. Also: add, commit, and push the project directory to your gitLab account including your lab .Rmd file.**

* **Make sure you change the author of the document to your name!**

# Problem 1


Load the `gapminder` dataset from the `gapminder` package. You will need to install the package once and load the package every time you compile the document



## Part a

Using the `tidyverse` functions from the `dplyr` package, check how many factors the `gapminder` dataset contains and how many levels each factor variable contains. Write out the solution using **complete sentences.**

### Solution
```{r}
gapminder
```
There are 2 factors, with 1704 levels to each factor. 









## Part b

Notice that Antarctica is not listed as a continent and is missing from the `continent` variable. Add in `Anartarctica` as a level of the factor for the `continent` variable in the `gapminder` dataset and save the modified data as `gapminder_updated`. (Hint, consider `fct_expand()`).

### Solution
```{r}
gapminder_updated <- gapminder%>%
  mutate(continent = fct_expand(continent, "Anartarctica"))
```













## Part c

Modify the `continent` factor in `gapminder_updated` by adding two new levels, `North-America` and `South-America` and removing the level `Americas`. The following countries from `Americas` should be in `South-America` and the rest of the countries should be in `North-America` and the result should be saved as `gapminder_updated`

```{r}
A <- c("Argentina", "Bolivia", "Brazil", "Chile", "Colombia", "Ecuador",
"Paraguay", "Peru", "Uruguay", "Venezuela")
```


### Solution
```{r}
gapminder_updated <- gapminder_updated%>%
  mutate(continent = fct_expand(continent, c("North-America", "South-America")))%>%
  mutate(continent = factor(case_when(
    country %in% A ~"South-America",
    continent == "Americas" ~"North-America",
    TRUE ~as.character(continent)), levels = c("Antartica", "Africa","Americas","Asia","Europe","Oceania", "North-America", "South-America")))
  
```












## Part d

Using the `gapmider_updated` data object with all seven continents, generate a boxplot of `lifeExp` for each continent as a function of time using `facet_wrap` (make sure there is only one row). Also, make sure the plot is clearly readable in your final document and that the axes are clearly labeled. Using **complete sentences**, what patterns do you see in this figure?


### Solution
```{r}
gapminder_updated%>%
  mutate(year = factor(year))%>%
  ggplot(aes(x = year, y = lifeExp))+
  geom_boxplot()+
  facet_wrap(~continent)
```


















# Problem 2

Using the `gss_cat` data from the `tidyverse` library, answer the following questions.



## Part a

What is the most common religion `relig`? Show code for how you found this and write out the answer in a **complete sentence**. 


### Solution
```{r}
gss_cat %>%
  group_by(relig)%>%
  summarise(N = n())%>%
  arrange(desc(N))
```
The most common religion is Protestant.

















## Part b

Plot the `marital` status from `gss_cat` using a barplot where the bars are arranged in decreasing order (Hint: get into increasing order then use `fct_rev()`). 

### Solution
```{r}
gss_cat%>%
  group_by(marital)%>%
  summarise(N = n()) %>%
  ggplot(aes(fct_rev(fct_reorder(marital, N)), y = N))+
  geom_col()
```
```{r}
gss_cat%>%
  group_by(marital)%>%
  summarise(N = n()) %>%
  ggplot(aes(fct_reorder(marital, N), y = N))+
  geom_col()
```



















## Part c
Now, determine how the proportions of people identifying as Democrat, Republican, and Independent has changed over time. To do this, collapse the `partyid` levels into `other`(No answer, Don't know, Other party), `republican` (Strong republican, Not str republican), `independent` (Ind,near rep, Independent, Ind,near dem), and `democrat` (Not str democrat, Strong democrat). Then, for each year, calculate the total count in each group as well as the total in the sample and then calculate the proportions from these totals. Finally, generate a line plot over time of the proportions that looks like the following:


```{r, out.width='100%', fig.align = 'center'}
knitr::include_graphics(here::here("images", "lab-14-party.png"))
```

### Solution
```{r}
gss_cat%>%
mutate(partyid = fct_collapse(partyid, 
      other = c("No answer", "Don't know", "Other party"),   
      republican = c("Strong republican", "Not str republican"), 
      independent = c("Ind,near rep", "Independent", "Ind,near dem"),
      democrat = c("Not str democrat", "Strong democrat")))%>%
  group_by(partyid, year)%>%
  summarise(N = n())%>%
  group_by(year)%>%
  summarise(p = N/sum(N), partyid)%>%
  ggplot(mapping = aes(x = year, y = p, group =  partyid, color = partyid))+
  geom_line(size = .3)+
  geom_point(size = .5)+
  labs(x = "year", y = "p",color = "Party ID.")+
  theme(axis.text=element_text(size=6), axis.title.x = element_text(size=7), axis.title.y = element_text(size=7),legend.key.size = unit(.3, 'cm'),
  legend.key.height = unit(.5, 'cm'), legend.key.width = unit(.4, 'cm'), 
  legend.title = element_text(size=7), legend.text = element_text(size=7))
  
```




















## Part d

Reproduce the following plot as best you can. Try to get all of the major features if possible:

```{r, out.width='100%', fig.align = 'center'}
knitr::include_graphics(here::here("images", "lab-14-income.png"))
```




### Solution
```{r}
gss_cat%>%
  filter(!rincome %in% c("Not applicable"))%>%
  mutate(rincome = fct_recode(rincome, "Less than $1000" = "Lt $1000"))%>%
  #mutate(shade = rincome %in% c("Refused", "Don't know", "No answer"))%>%
  group_by(rincome)%>%
  summarise(N = n())%>%
  ggplot(aes(x = N, y = rincome))+
  geom_col()+
  xlab("Number of Respondents")+
  ylab("Respondent's Income")
  

  
```










